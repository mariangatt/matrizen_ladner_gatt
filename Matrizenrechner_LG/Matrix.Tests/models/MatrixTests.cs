﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix.models.Exceptions;
using NUnit.Framework;
namespace Matrix.Tests
{

    [TestFixture]
    class MatrixTests
    {
        readonly double DELTA = 0.01;
        [Test]
        public void StdCtor_ShouldSetRowsAndColumnsToZero()
        {
            Matrix StdMatrix = new Matrix();

            Assert.AreEqual(StdMatrix[0, 0], 0);
            Assert.AreEqual(StdMatrix.Rows, 1);
            Assert.AreEqual(StdMatrix.Columns, 1);
        }

        [TestCase(3, 1, 3)]
        [TestCase(1, 3, 3)]
        [TestCase(3, 3, 9)]
        [TestCase(100, 100, 100 * 100)]
        public void Ctor_ShouldCreateCorrectMatrix(int rows, int columns, int CountOfElemnts)
        {
            Matrix CtorMatrix = new Matrix(rows, columns);
            int CountOfElemtsCalculated = CtorMatrix.Rows * CtorMatrix.Columns;
            Assert.AreEqual(CountOfElemtsCalculated, CountOfElemnts);
        }
        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(0, 1)]
        [TestCase(-1, 1)]
        [TestCase(1, -1)]
        public void Ctor_ImpossibleMatrixException(int rows, int columns)
        {
            Assert.Throws<ImpossibleMatrixException>(() => { Matrix CtorMatrix = new Matrix(rows, columns); });
        }

        [TestCase(3, 3)]
        [TestCase(90, 3)]
        public void PropertyRowAndColumn_ShouldReturnCorrectValue(int rows, int columns)
        {
            Matrix m1 = new Matrix(rows, columns);

            Assert.AreEqual(m1.Rows, rows);
            Assert.AreEqual(m1.Columns, columns);
        }
        [TestCase(3, 2, 1)]
        public void Indexer_ShouldSetCorrectValues(double value, int row, int column)
        {
            Matrix m1 = new Matrix(3, 3);

            m1[row, column] = value;

            Assert.AreEqual(m1[row, column], value);
        }

        [TestCase(1, 3, 1, 1, 3, 4, -3, -0, 123, 3, 4, -123, 123, 32, 4, 123, 132, 2)]
        [TestCase(0, 2, 0, 0, 3, 0, 13, -10, -3, 3, 0, -123, 0, 32, 0, 123, 132, 2)]
        public void PlusOperator_ShouldCalculateCorrectResults(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double vM2R1C1, double vM2R1C2, double vM2R1C3, double vM2R2C1, double vM2R2C2, double vM2R2C3, double vM2R3C1, double vM2R3C2, double vM2R3C3)
        {
            Matrix m1 = new Matrix(3, 3);
            Matrix m2 = new Matrix(3, 3);
            Matrix result;
            Matrix ExpectedMatrix = new Matrix(3, 3);

            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;

            //m2
            m2[0, 0] = vM2R1C1;
            m2[0, 1] = vM2R1C2;
            m2[0, 2] = vM2R1C3;

            m2[1, 0] = vM2R2C1;
            m2[1, 1] = vM2R2C2;
            m2[1, 2] = vM2R2C3;

            m2[2, 0] = vM2R3C1;
            m2[2, 1] = vM2R3C2;
            m2[2, 2] = vM2R3C3;

            result = m1 + m2;

            ExpectedMatrix[0, 0] = vM1R1C1 + vM2R1C1;
            ExpectedMatrix[0, 1] = vM1R1C2 + vM2R1C2;
            ExpectedMatrix[0, 2] = vM1R1C3 + vM2R1C3;

            ExpectedMatrix[1, 0] = vM1R2C1 + vM2R2C1;
            ExpectedMatrix[1, 1] = vM1R2C2 + vM2R2C2;
            ExpectedMatrix[1, 2] = vM1R2C3 + vM2R2C3;

            ExpectedMatrix[2, 0] = vM1R3C1 + vM2R3C1;
            ExpectedMatrix[2, 1] = vM1R3C2 + vM2R3C2;
            ExpectedMatrix[2, 2] = vM1R3C3 + vM2R3C3;

            Assert.AreEqual(ExpectedMatrix.data, result.data);
        }

        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 22, 28, 49, 64, 76, 100)]
        public void MultiplicationOperator_ShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double vM2R1C1, double vM2R1C2, double vM2R2C1, double vM2R2C2, double vM2R3C1, double vM2R3C2, double vERR1C1, double vERR1C2, double vERR2C1, double vERR2C2, double vERR3C1, double vERR3C2)
        {
            Matrix m1 = new Matrix(3, 3);
            Matrix m2 = new Matrix(3, 2);
            Matrix result;
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;

            //m2
            m2[0, 0] = vM2R1C1;
            m2[0, 1] = vM2R1C2;

            m2[1, 0] = vM2R2C1;
            m2[1, 1] = vM2R2C2;

            m2[2, 0] = vM2R3C1;
            m2[2, 1] = vM2R3C2;

            result = m1 * m2;
            Matrix expectedResult = new Matrix(3, 2);
            expectedResult[0, 0] = vERR1C1;
            expectedResult[0, 1] = vERR1C2;

            expectedResult[1, 0] = vERR2C1;
            expectedResult[1, 1] = vERR2C2;

            expectedResult[2, 0] = vERR3C1;
            expectedResult[2, 1] = vERR3C2;

            Assert.AreEqual(expectedResult.data, result.data);
        }

        [TestCase(1, 3, 1, 1, 3, 4, -3, -0, 123, 3, 4, -123, 123, 32, 4, 123, 132, 2)]
        [TestCase(0, 2, 0, 0, 3, 0, 13, -10, -3, 3, 0, -123, 0, 32, 0, 123, 132, 2)]
        public void MinusOperator_ShouldCalculateCorrectResults(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double vM2R1C1, double vM2R1C2, double vM2R1C3, double vM2R2C1, double vM2R2C2, double vM2R2C3, double vM2R3C1, double vM2R3C2, double vM2R3C3)
        {
            Matrix m1 = new Matrix(3, 3);
            Matrix m2 = new Matrix(3, 3);
            Matrix result;
            Matrix ExpectedMatrix = new Matrix(3, 3);

            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;

            //m2
            m2[0, 0] = vM2R1C1;
            m2[0, 1] = vM2R1C2;
            m2[0, 2] = vM2R1C3;

            m2[1, 0] = vM2R2C1;
            m2[1, 1] = vM2R2C2;
            m2[1, 2] = vM2R2C3;

            m2[2, 0] = vM2R3C1;
            m2[2, 1] = vM2R3C2;
            m2[2, 2] = vM2R3C3;

            result = m1 - m2;

            ExpectedMatrix[0, 0] = vM1R1C1 - vM2R1C1;
            ExpectedMatrix[0, 1] = vM1R1C2 - vM2R1C2;
            ExpectedMatrix[0, 2] = vM1R1C3 - vM2R1C3;

            ExpectedMatrix[1, 0] = vM1R2C1 - vM2R2C1;
            ExpectedMatrix[1, 1] = vM1R2C2 - vM2R2C2;
            ExpectedMatrix[1, 2] = vM1R2C3 - vM2R2C3;

            ExpectedMatrix[2, 0] = vM1R3C1 - vM2R3C1;
            ExpectedMatrix[2, 1] = vM1R3C2 - vM2R3C2;
            ExpectedMatrix[2, 2] = vM1R3C3 - vM2R3C3;

            Assert.AreEqual(ExpectedMatrix.data, result.data);

        }


        [TestCase(3, 3, 2, 3)]
        [TestCase(2, 3, 2, 2)]
        public void PlusOperatorThrowsExceptionCorrect(int m1Rows, int m1columns, int m2Rows, int m2columns)
        {
            Matrix firstMatrix = new Matrix(m1Rows, m1columns);
            Matrix secondMatrix = new Matrix(m2Rows, m2columns);
            Assert.Throws<ImpossibleCalculationException>(() => { Matrix result = firstMatrix + secondMatrix; });

        }

        [TestCase(6, 6, 3, 2)]
        [TestCase(3, 2, 6, 6)]
        public void MinusOperatorThrowsExceptionCorrect(int m1rows, int m1columns, int m2rows, int m2columns)
        {
            Matrix firstMatrix = new Matrix(m1rows, m1columns);
            Matrix secondMatrix = new Matrix(m2rows, m2columns);
            Assert.Throws<ImpossibleCalculationException>(() => { Matrix result = firstMatrix - secondMatrix; });
        }


        [TestCase(2, 2, 3, 3)]
        [TestCase(3, 3, 4, 2)]
        public void MultiplikationOperatorThrowsEcxeptionCorrect(int m1rows, int m1columns, int m2Rows, int m2columns)
        {
            Matrix firstmatrix = new Matrix(m1rows, m1columns);
            Matrix secondMatrix = new Matrix(m2Rows, m2columns);
            Assert.Throws<ImpossibleCalculationException>(() => { Matrix result = firstmatrix * secondMatrix; });
        }


        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 4, 7, 2, 5, 8, 3, 6, 9)]
        public void Matrix_transposedMatrixShouldReturnCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double vM2R1C1, double vM2R1C2, double vM2R1C3, double vM2R2C1, double vM2R2C2, double vM2R2C3, double vM2R3C1, double vM2R3C2, double vM2R3C3)
        {
            Matrix m1 = new Matrix(3, 3);
            Matrix m2 = new Matrix(3, 3);

            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;

            //m2
            m2[0, 0] = vM2R1C1;
            m2[0, 1] = vM2R1C2;
            m2[0, 2] = vM2R1C3;

            m2[1, 0] = vM2R2C1;
            m2[1, 1] = vM2R2C2;
            m2[1, 2] = vM2R2C3;

            m2[2, 0] = vM2R3C1;
            m2[2, 1] = vM2R3C2;
            m2[2, 2] = vM2R3C3;

            Matrix result = m1.transposedMatrix();
            Assert.AreEqual(m2.data, result.data);
        }

        [TestCase(6, 7, 8, 3, 2, 4, 6, 3, 4, 4, 7, 3, 9, 2, 6, 7, 8, 1, 38, 66, 91, 74, 61, 38, 50, 77, 48, 75, 16, 32, 42, 38, 22, 30, 42, 63, 42, 57)]
        public void MultiplikationShouldWorkWithDifferentMatrixSizes(double vM1R1C1, double vM1R1C2, double vM1R2C1, double vM1R2C2, double vM1R3C1, double vM1R3C2, double vM1R4C1, double vM1R4C2, double vM2R1C1, double vM2R1C2, double vM2R1C3, double vM2R1C4, double vM2R1C5, double vM2R2C1, double vM2R2C2, double vM2R2C3, double vM2R2C4, double vM2R2C5, double vERR1C1, double vERR1C2, double vERR1C3, double vERR1C4, double vERR1C5, double vERR2C1, double vERR2C2, double vERR2C3, double vERR2C4, double vERR2C5, double vERR3C1, double vERR3C2, double vERR3C3, double vERR3C4, double vERR3C5, double vERR4C1, double vERR4C2, double vERR4C3, double vERR4C4, double vERR4C5)
        {
            Matrix m1 = new Matrix(4, 2);
            Matrix m2 = new Matrix(2, 5);
            Matrix result;

            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[3, 0] = vM1R4C1;
            m1[3, 1] = vM1R4C2;


            m2[0, 0] = vM2R1C1;
            m2[0, 1] = vM2R1C2;
            m2[0, 2] = vM2R1C3;
            m2[0, 3] = vM2R1C4;
            m2[0, 4] = vM2R1C5;
            m2[1, 0] = vM2R2C1;
            m2[1, 1] = vM2R2C2;
            m2[1, 2] = vM2R2C3;
            m2[1, 3] = vM2R2C4;
            m2[1, 4] = vM2R2C5;


            result = m1 * m2;
            Matrix expectedMatrix = new Matrix(4, 5);
            expectedMatrix[0, 0] = vERR1C1;
            expectedMatrix[0, 1] = vERR1C2;
            expectedMatrix[0, 2] = vERR1C3;
            expectedMatrix[0, 3] = vERR1C4;
            expectedMatrix[0, 4] = vERR1C5;
            expectedMatrix[1, 0] = vERR2C1;
            expectedMatrix[1, 1] = vERR2C2;
            expectedMatrix[1, 2] = vERR2C3;
            expectedMatrix[1, 3] = vERR2C4;
            expectedMatrix[1, 4] = vERR2C5;
            expectedMatrix[2, 0] = vERR3C1;
            expectedMatrix[2, 1] = vERR3C2;
            expectedMatrix[2, 2] = vERR3C3;
            expectedMatrix[2, 3] = vERR3C4;
            expectedMatrix[2, 4] = vERR3C5;
            expectedMatrix[3, 0] = vERR4C1;
            expectedMatrix[3, 1] = vERR4C2;
            expectedMatrix[3, 2] = vERR4C3;
            expectedMatrix[3, 3] = vERR4C4;
            expectedMatrix[3, 4] = vERR4C5;


            Assert.AreEqual(expectedMatrix.data, result.data);

        }

        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 2, 2, 4, 6, 8, 10, 12, 14, 16, 18)]
        public void Matrix_MultiplicationWithScalar(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double ScalarValue, double vERR1C1, double vERR1C2, double vERR1C3, double vERR2C1, double vERR2C2, double vERR2C3, double vERR3C1, double vERR3C2, double vERR3C3)
        {
            Matrix m1 = new Matrix(3, 3);
            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;

            Matrix result = m1;
            result[0, 0] = result[0, 0] * ScalarValue;
            result[0, 1] = result[0, 1] * ScalarValue;
            result[0, 2] = result[0, 2] * ScalarValue;

            result[1, 0] = result[1, 0] * ScalarValue;
            result[1, 1] = result[1, 1] * ScalarValue;
            result[1, 2] = result[1, 2] * ScalarValue;

            result[2, 0] = result[2, 0] * ScalarValue;
            result[2, 1] = result[2, 1] * ScalarValue;
            result[2, 2] = result[2, 2] * ScalarValue;

            Matrix ExpectedResult = new Matrix(3, 3);
            ExpectedResult[0, 0] = vERR1C1;
            ExpectedResult[0, 1] = vERR1C2;
            ExpectedResult[0, 2] = vERR1C3;

            ExpectedResult[1, 0] = vERR2C1;
            ExpectedResult[1, 1] = vERR2C2;
            ExpectedResult[1, 2] = vERR2C3;

            ExpectedResult[2, 0] = vERR3C1;
            ExpectedResult[2, 1] = vERR3C2;
            ExpectedResult[2, 2] = vERR3C3;

            Assert.AreEqual(ExpectedResult.data, result.data);


        }

        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 3)]
        public void Matrix_TranslationXShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double MoveXValue)
        {
            Matrix m1 = new Matrix(3, 4);
            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;
            m1[0, 3] = 1;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;
            m1[1, 3] = 1;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;
            m1[2, 3] = 1;

            Matrix result = m1.TranslationX(MoveXValue);
            Matrix ExpectedResult = m1;
            ExpectedResult[0, 0] += MoveXValue;
            ExpectedResult[1, 0] += MoveXValue;
            ExpectedResult[2, 0] += MoveXValue;
            Assert.AreEqual(ExpectedResult.data, result.data);

        }

        [TestCase(1,3)]
        public void ExceptionForTranslationXThrowCorrect(int m1rows, int m1columns)
        {
            Matrix firstmatrix = new Matrix(m1rows, m1columns);
            Assert.Throws<ImpossibleTranslation>(() => { firstmatrix.TranslationX(1); });
        }

        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 3)]
        public void Matrix_TranslationYShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double MoveYValue)
        {
            Matrix m1 = new Matrix(3, 4);
            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;
            m1[0, 3] = 1;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;
            m1[1, 3] = 1;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;
            m1[2, 3] = 1;

            Matrix result = m1.TranslationY(MoveYValue);
            Matrix ExpectedResult = m1;
            ExpectedResult[0, 1] += MoveYValue;
            ExpectedResult[1, 1] += MoveYValue;
            ExpectedResult[2, 1] += MoveYValue;
            Assert.AreEqual(ExpectedResult.data, result.data);
        }

        [TestCase(2,3)]
        public void ExceptionForTranslationYThrowCorrect(int m1rows, int m1columns)
        {
            Matrix firstmatrix = new Matrix(m1rows, m1columns);
            Assert.Throws<ImpossibleTranslation>(() => { firstmatrix.TranslationY(1); });
        }


        [TestCase(1, 2, 3, 4, 5, 6, 7, 8, 9, 3)]
        public void Matrix_TranslationZShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double vM1R3C1, double vM1R3C2, double vM1R3C3, double MoveZValue)
        {
            Matrix m1 = new Matrix(3, 4);
            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;
            m1[0, 3] = 1;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;
            m1[1, 3] = 1;

            m1[2, 0] = vM1R3C1;
            m1[2, 1] = vM1R3C2;
            m1[2, 2] = vM1R3C3;
            m1[2, 3] = 1;

            Matrix result = m1.TranslationZ(MoveZValue);
            Matrix ExpectedResult = m1;
            ExpectedResult[0, 2] += MoveZValue;
            ExpectedResult[1, 2] += MoveZValue;
            ExpectedResult[2, 2] += MoveZValue;
            Assert.AreEqual(ExpectedResult.data, result.data);
        }

        [TestCase(1, 2)]
        public void ExceptionForTranslationZThrowCorrect(int m1rows, int m1columns)
        {
            Matrix firstmatrix = new Matrix(m1rows, m1columns);
            Assert.Throws<ImpossibleTranslation>(() => { firstmatrix.TranslationZ(1); });
        }

        [TestCase(1, 2, 3, 1, 3, 2, 90, 1, 3, -2, 1, 2, -3)]
        public void Matrix_RotationXShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double WinkelInRad, double vERR1C1, double vERR1C2, double vERR1C3, double vERR2C1, double vERR2C2, double vERR2C3)
        {
            Matrix m1 = new Matrix(2, 3);
            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;

            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            Matrix ExpectedResult = new Matrix(2, 3);
            ExpectedResult[0, 0] = vERR1C1;
            ExpectedResult[0, 1] = vERR1C2;
            ExpectedResult[0, 2] = vERR1C3;

            ExpectedResult[1, 0] = vERR2C1;
            ExpectedResult[1, 1] = vERR2C2;
            ExpectedResult[1, 2] = vERR2C3;

            Matrix result = m1.RotationX(WinkelInRad);

            for (int row = 0; row < result.Rows; row++)
            {
                for (int column = 0; column < result.Columns; column++)
                {
                    Assert.AreEqual(ExpectedResult[row, column], result[row, column], DELTA);
                }
            }
        }


        [TestCase(2,3,4,5,6,7,8)]
        public void Matrix_ScalingXShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double factor)
        {
            Matrix m1 = new Matrix(2, 3);

            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;
            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            Matrix ExcpectedResult = m1;
            m1[0, 0] *= factor;
            m1[1, 0] *= factor;

            m1.ScalingX(factor);

            Assert.AreEqual(ExcpectedResult.data, m1.data);
        }

        [TestCase(-2)]
        public void ExceptionForScalingXShouldThrownCorrect(double xFactor)
        {
            Matrix m1 = new Matrix(3, 3);
            Assert.Throws<ImpossibleScaling>(() => { m1.ScalingX(xFactor); });
        }

        [TestCase(2, 3, 4, 5, 6, 7, 8)]
        public void Matrix_ScalingYShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double factor)
        {
            Matrix m1 = new Matrix(2, 3);

            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;
            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            Matrix ExcpectedResult = m1;
            m1[0, 1] *= factor;
            m1[1, 1] *= factor;

            m1.ScalingY(factor);

            Assert.AreEqual(ExcpectedResult.data, m1.data);

        }

        [TestCase(-2)]
        public void ExceptionForScalingYShouldThrownCorrect(double yFactor)
        {
            Matrix m1 = new Matrix(3, 3);
            Assert.Throws<ImpossibleScaling>(() => { m1.ScalingY(yFactor); });
        }

        [TestCase(2, 3, 4, 5, 6, 7, 8)]
        public void Matrix_ScalingZShouldCalculateCorrectValues(double vM1R1C1, double vM1R1C2, double vM1R1C3, double vM1R2C1, double vM1R2C2, double vM1R2C3, double factor)
        {
            Matrix m1 = new Matrix(2, 3);

            //m1
            m1[0, 0] = vM1R1C1;
            m1[0, 1] = vM1R1C2;
            m1[0, 2] = vM1R1C3;
            m1[1, 0] = vM1R2C1;
            m1[1, 1] = vM1R2C2;
            m1[1, 2] = vM1R2C3;

            Matrix ExcpectedResult = m1;
            m1[0, 2] *= factor;
            m1[1, 2] *= factor;

            m1.ScalingZ(factor);

            Assert.AreEqual(ExcpectedResult.data, m1.data);

        }
        [TestCase(-2)]
        public void ExceptionForScalingZShouldThrownCorrect(double zFactor)
        {
            Matrix m1 = new Matrix(3, 3);
            Assert.Throws<ImpossibleScaling>(() => { m1.ScalingZ(zFactor); });
        }


    }
}

