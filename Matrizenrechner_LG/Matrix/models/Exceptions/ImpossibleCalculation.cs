﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.models.Exceptions
{

    [Serializable]
    public class ImpossibleCalculationException : Exception
    {
        public ImpossibleCalculationException() { }
        public ImpossibleCalculationException(string message) : base(message) { }
        public ImpossibleCalculationException(string message, Exception inner) : base(message, inner) { }
        protected ImpossibleCalculationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
