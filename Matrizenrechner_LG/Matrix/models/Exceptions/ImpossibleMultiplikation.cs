﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.models.Exceptions
{

    [Serializable]
    public class ImpossibleMultiplikation : Exception
    {
        public ImpossibleMultiplikation() { }
        public ImpossibleMultiplikation(string message) : base(message) { }
        public ImpossibleMultiplikation(string message, Exception inner) : base(message, inner) { }
        protected ImpossibleMultiplikation(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
