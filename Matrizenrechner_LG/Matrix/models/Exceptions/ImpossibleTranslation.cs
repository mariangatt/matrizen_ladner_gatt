﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.models.Exceptions
{

    [Serializable]
    public class ImpossibleTranslation : Exception
    {
        public ImpossibleTranslation() { }
        public ImpossibleTranslation(string message) : base(message) { }
        public ImpossibleTranslation(string message, Exception inner) : base(message, inner) { }
        protected ImpossibleTranslation(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
