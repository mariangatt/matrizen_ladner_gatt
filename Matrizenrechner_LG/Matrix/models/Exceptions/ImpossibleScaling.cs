﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.models.Exceptions
{

    [Serializable]
    public class ImpossibleScaling : Exception
    {
        public ImpossibleScaling() { }
        public ImpossibleScaling(string message) : base(message) { }
        public ImpossibleScaling(string message, Exception inner) : base(message, inner) { }
        protected ImpossibleScaling(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
