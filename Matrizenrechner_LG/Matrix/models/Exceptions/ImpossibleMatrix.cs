﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix.models.Exceptions
{

    [Serializable]
    public class ImpossibleMatrixException : Exception
    {
        public ImpossibleMatrixException() { }
        public ImpossibleMatrixException(string message) : base(message) { }
        public ImpossibleMatrixException(string message, Exception inner) : base(message, inner) { }
        protected ImpossibleMatrixException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
