﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix.models.Exceptions;
namespace Matrix
{
    public class Matrix
    {
        public double[,] data;

        public int Rows
        {
            get { return this.data.GetLength(0); }
        }

        public int Columns
        {
            get { return this.data.GetLength(1); }
        }

        public double this[int row, int column]
        {
            get { return data[row, column]; }
            set
            {
                data[row, column] = value;
            }
        }
        public Matrix() : this(1, 1) { }
        public Matrix(int row, int column)
        {
            if ((row < 1) || (column < 1))
                throw new ImpossibleMatrixException();

            this.data = new double[row, column];
            for (int currentRows = 0; currentRows < this.Rows; currentRows++)
            {
                for (int currentColumn = 0; currentColumn < this.Columns; currentColumn++)
                {
                    data[currentRows, currentColumn] = 0;
                }
            }

        }
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if ((m1.Rows != m2.Rows) || (m1.Columns != m2.Columns))
            {
                throw new ImpossibleCalculationException();
            }

            Matrix result = m1;
            for (int currentRow = 0; currentRow < m1.Rows; currentRow++)
            {
                for (int currentColumn = 0; currentColumn < m1.Columns; currentColumn++)
                {
                    result[currentRow, currentColumn] += m2[currentRow, currentColumn];
                }
            }
            return result;
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if ((m1.Rows != m2.Rows) || (m1.Columns != m2.Columns))
            {
                throw new ImpossibleCalculationException();
            }

            Matrix result = m1;
            for (int currentRow = 0; currentRow < m1.Rows; currentRow++)
            {
                for (int currentColumn = 0; currentColumn < m1.Columns; currentColumn++)
                {
                    result[currentRow, currentColumn] -= m2[currentRow, currentColumn];
                }
            }

            return result;

        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            Matrix result = new Matrix(m1.Rows, m2.Columns);
            double mathValue;

            if ((m1.Rows != m2.Columns) && (m1.Columns != m2.Rows))
            {
                throw new ImpossibleCalculationException();
            }
            for (int cResultRow = 0; cResultRow < result.Rows; cResultRow++)
            {
                for (int cResultColumn = 0; cResultColumn < result.Columns; cResultColumn++)
                {
                    mathValue = 0;
                    for (int x = 0; x < m1.Columns; x++)
                    {
                        mathValue += m1[cResultRow, x] * m2[x, cResultColumn];
                    }
                    result[cResultRow, cResultColumn] = mathValue;
                }
            }

            return result;
        }
        public Matrix transposedMatrix()
        {
            Matrix result = new Matrix(this.Columns, this.Rows);
            for (int r = 0; r < this.Rows; r++)
            {
                for (int c = 0; c < this.Columns; c++)
                {
                    result[c, r] = this[r, c];
                }
            }
            return result;
        }

        public Matrix MultiplyWithScalar(double ScalarValue)
        {
            Matrix result = this;
            for (int r = 0; r < this.Rows; r++)
            {
                for (int c = 0; c < this.Columns; c++)
                {
                    result[r, c] = this[r, c] * ScalarValue;
                }
            }
            return result;
        }

        public Matrix TranslationX(double x)
        {
            Matrix result = this;
            if (this.Columns != 4)
            {
                throw new ImpossibleTranslation();
            }
            Matrix MoveX = new Matrix(4, 4);
            MoveX[0, 0] = 1;
            MoveX[1, 1] = 1;
            MoveX[2, 2] = 1;
            MoveX[3, 3] = 1;
            MoveX[3, 0] = x;

            result = result * MoveX;

            return result;
        }

        public Matrix TranslationY(double y)
        {
            Matrix result = this;
            if (this.Columns != 4)
            {
                throw new ImpossibleTranslation();
            }
            Matrix MoveY = new Matrix(4, 4);
            MoveY[0, 0] = 1;
            MoveY[1, 1] = 1;
            MoveY[2, 2] = 1;
            MoveY[3, 3] = 1;
            MoveY[3, 1] = y;

            result = result * MoveY;

            return result;
        }
        public Matrix TranslationZ(double z)
        {
            Matrix result = this;
            if (this.Columns != 4)
            {
                throw new ImpossibleTranslation();
            }
            Matrix MoveZ = new Matrix(4, 4);
            MoveZ[0, 0] = 1;
            MoveZ[1, 1] = 1;
            MoveZ[2, 2] = 1;
            MoveZ[3, 3] = 1;
            MoveZ[3, 2] = z;

            result = result * MoveZ;

            return result;
        }

        public Matrix RotationX(double Winkel)
        {
            Matrix result = new Matrix(this.Rows, this.Columns);
            Matrix TransposedMatrix = this.transposedMatrix();
            Matrix RotationXMatrix = new Matrix(3, 3);
            RotationXMatrix[0, 0] = 1;
            RotationXMatrix[1, 1] = Math.Cos((Winkel / 180) * Math.PI);
            RotationXMatrix[2, 2] = Math.Cos((Winkel / 180) * Math.PI);
            RotationXMatrix[1, 2] = (Math.Sin((Winkel / 180) * Math.PI));
            RotationXMatrix[2, 1] = -(Math.Sin((Winkel / 180) * Math.PI));

            Matrix OnePoint = new Matrix(3, 1);
            for (int x = 0; x < this.Rows; x++)
            {
                OnePoint[0, 0] = TransposedMatrix[0, x];
                OnePoint[1, 0] = TransposedMatrix[1, x];
                OnePoint[2, 0] = TransposedMatrix[2, x];

                OnePoint = RotationXMatrix * OnePoint;

                result[x, 0] = OnePoint[0, 0];
                result[x, 1] = OnePoint[1, 0];
                result[x, 2] = OnePoint[2, 0];
            }

            return result;
        }

        public Matrix RotationY(double Winkel)
        {
            Matrix result = new Matrix(this.Rows, this.Columns);
            Matrix TransposedMatrix = this.transposedMatrix();
            Matrix RotationYMatrix = new Matrix(3, 3);
            RotationYMatrix[1, 1] = 1;
            RotationYMatrix[0, 0] = Math.Cos((Winkel / 180) * Math.PI);
            RotationYMatrix[3, 3] = Math.Cos((Winkel / 180) * Math.PI);
            RotationYMatrix[3, 1] = (Math.Sin((Winkel / 180) * Math.PI));
            RotationYMatrix[1, 3] = -(Math.Sin((Winkel / 180) * Math.PI));

            Matrix OnePoint = new Matrix(3, 1);
            for (int x = 0; x < this.Rows; x++)
            {
                OnePoint[0, 0] = TransposedMatrix[0, x];
                OnePoint[1, 0] = TransposedMatrix[1, x];
                OnePoint[2, 0] = TransposedMatrix[2, x];

                OnePoint = RotationYMatrix * OnePoint;

                result[x, 0] = OnePoint[0, 0];
                result[x, 1] = OnePoint[1, 0];
                result[x, 2] = OnePoint[2, 0];
            }

            return result;
        }

        public Matrix RotationZ(double Winkel)
        {
            Matrix result = new Matrix(this.Rows, this.Columns);
            Matrix TransposedMatrix = this.transposedMatrix();
            Matrix RotationZMatrix = new Matrix(3, 3);
            RotationZMatrix[3, 3] = 1;
            RotationZMatrix[0, 0] = Math.Cos((Winkel / 180) * Math.PI);
            RotationZMatrix[2, 2] = Math.Cos((Winkel / 180) * Math.PI);
            RotationZMatrix[1, 2] = (Math.Sin((Winkel / 180) * Math.PI));
            RotationZMatrix[2, 1] = -(Math.Sin((Winkel / 180) * Math.PI));

            Matrix OnePoint = new Matrix(3, 1);
            for (int x = 0; x < this.Rows; x++)
            {
                OnePoint[0, 0] = TransposedMatrix[0, x];
                OnePoint[1, 0] = TransposedMatrix[1, x];
                OnePoint[2, 0] = TransposedMatrix[2, x];

                OnePoint = RotationZMatrix * OnePoint;

                result[x, 0] = OnePoint[0, 0];
                result[x, 1] = OnePoint[1, 0];
                result[x, 2] = OnePoint[2, 0];
            }

            return result;
        }

        public Matrix ScalingX(double xFactor)
        {
            Matrix result = this;
            if (xFactor < 0)
            {
                throw new ImpossibleScaling();
            }

            for (int r = 0; r < this.Rows; r++)
            {
                result[r, 0] = this[r, 0] * xFactor;
            }

            return result;
        }

        public Matrix ScalingY(double yFactor)
        {
            Matrix result = this;
            if (yFactor < 0)
            {
                throw new ImpossibleScaling();
            }

            for (int r = 0; r < this.Rows; r++)
            {
                result[r, 1] = this[r, 1] * yFactor;
            }

            return result;
        }

        public Matrix ScalingZ(double zFactor)
        {
            Matrix result = this;
            if (zFactor < 0)
            {
                throw new ImpossibleScaling();
            }

            for (int r = 0; r < this.Rows; r++)
            {
                result[r, 1] = this[r, 1] * zFactor;
            }

            return result;
        }

        public Matrix Rotation(Matrix Axis, double Angle)
        {
            Matrix result = this;
            //Werte abspeichern
            //x
            double Axis_x = Axis[0, 0];
            double Matrix_x = result[0, 0];
            //y
            double Axis_y = Axis[0, 1];
            double Matrix_y = result[0, 1];
            //z
            double Axis_z = Axis[0, 2];
            double Matrix_z = result[0, 2];


            //-X
            Axis.TranslationX(-Axis_x);
            result.TranslationX(-Matrix_x);
            //-Y
            Axis.TranslationY(-Axis_y);
            result.TranslationY(-Matrix_y);
            //-Z
            Axis.TranslationZ(-Axis_z);
            result.TranslationZ(-Matrix_z);
            //Winkel
            double Angle2 = Math.Atan(Axis[1, 3] / Axis[1, 0]);
            double Angle3 = Math.Atan(Axis[1, 2] / Axis[1, 0]);
            //Ry(a1)
            Axis.RotationY(Angle2);
            this.RotationY(Angle2);
            //Rz(a2)
            Axis.RotationZ(Angle3);
            this.RotationZ(Angle3);
            //Rx(a)
            Axis.RotationX(Angle);
            this.RotationX(Angle);

            //Ry(-a1)
            Axis.RotationY(-Angle2);
            this.RotationY(-Angle2);
            //Rz(-a2)
            Axis.RotationZ(-Angle3);
            this.RotationZ(-Angle3);

            //X
            Axis.TranslationX(Axis_x);
            result.TranslationX(Matrix_x);
            //Y
            Axis.TranslationY(Axis_y);
            result.TranslationY(Matrix_y);
            //Z
            Axis.TranslationZ(Axis_z);
            result.TranslationZ(Matrix_z);



            return result;

        }

    }

}
